﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

using System.Threading;

using Tools.FileModifyWatcher.ViewModel;
using Tools.FileModifyWatcher.View;

namespace Tools.FileModifyWatcher {
	/// <summary>
	/// App.xaml の相互作用ロジック
	/// </summary>
	public partial class App : Application {
		private void Application_Startup(object sender, StartupEventArgs e) {
			// 多重起動チェック
			App.Mutex = new Mutex(false, App.MutexName);
			if (!App.Mutex.WaitOne(0, false)) {
				App.Mutex.Close();
				App.Mutex = null;
				this.Shutdown();
				return;
			}

			var window = new MainWindow();
			window.DataContext = new MainWindowViewModel(window);
			window.Show();
		}

		private void Application_Exit(object sender, ExitEventArgs e) {
			if (App.Mutex == null) { return; }

			// ミューテックスの解放
			App.Mutex.ReleaseMutex();
			App.Mutex.Close();
			App.Mutex = null;
		}

		/// <summary>
		/// 多重起動を防止するためのミューテックス名。
		/// </summary>
		private const string MutexName = "{7E9DF3FF-ECFC-4EE0-AA5D-B88A806E305E}";

		/// <summary>
		/// 多重起動を防止する為のミューテックス。
		/// </summary>
		private static Mutex Mutex;
	}
}
