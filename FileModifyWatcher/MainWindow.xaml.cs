﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Tools.FileModifyWatcher.ViewModel;

namespace Tools.FileModifyWatcher {
	/// <summary>
	/// MainWindow.xaml の相互作用ロジック
	/// </summary>
	public partial class MainWindow : Window {
		public MainWindow() {
			InitializeComponent();
			

		}
		private void Window_Loaded(object sender, RoutedEventArgs e) {
		}

		private void button1_Click(object sender, RoutedEventArgs e) {
		}

		private void button2_Click(object sender, RoutedEventArgs e) {
		}

		private void button3_Click(object sender, RoutedEventArgs e) {

		}

		protected override void  OnClosing(System.ComponentModel.CancelEventArgs e){
			var context = this.DataContext as MainWindowViewModel;
			if (context != null) {
				context.Dispose();
			}

			base.OnClosing(e);
		}



	}
}
