﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

using Tools.FileModifyWatcher.Model;

using System.Windows.Input;

namespace Tools.FileModifyWatcher.ViewModel {
	public class FolderSettingViewModel : ViewModelBase {
		WatchingFolderDescription _watchingFolder;
		ObservableCollection<FolderSettingViewModel> _list;

		public string WatchPath {
			get {
				return _watchingFolder.WatchPath;
			}
		}

		public bool IsEnable {
			get {
				return _watchingFolder.IsEnable;
			}
			set {
				_watchingFolder.IsEnable = value;
			}
		}

		public bool IsSubDirectory {
			get {
				return _watchingFolder.IsSubDirectory;
			}
			set {
				_watchingFolder.IsSubDirectory = value;
			}
		}
		public bool IsNotifyChange {
			get {
				return _watchingFolder.IsNotifyChange;
			}
			set {
				_watchingFolder.IsNotifyChange = value;
			}

		}
		public bool IsNotifyCreate {
			get {
				return _watchingFolder.IsNotifyCreate;
			}
			set {
				_watchingFolder.IsNotifyCreate = value;
			}

		}
		public bool IsNotifyDelete {
			get {
				return _watchingFolder.IsNotifyDelete;
			}
			set {
				_watchingFolder.IsNotifyDelete = value;
			}

		}
		public bool IsNotifyRename {
			get {
				return _watchingFolder.IsNotifyRename;
			}
			set {
				_watchingFolder.IsNotifyRename = value;
			}

		}

		public FolderSettingViewModel(WatchingFolderDescription watchingFolder,
			ObservableCollection<FolderSettingViewModel> list) {
			this._list = list;
			this._watchingFolder = watchingFolder;
			this.DeleteCommand = new Command.DelegateCommand(
				() => {
					_list.Remove(this);
				}
			, null);

			this.ModifyFilterCommand = new Command.DelegateCommand(
				() => {
					var filterWindow = new View.FilterListWindowView();
					var filterDictionary = Model.WatcherComponent.Instance.FilterDictionary;
					ObservableCollection<FilterDescription> filters;
					if (filterDictionary.TryGetValue(this._watchingFolder.WatchPath, out filters)) {
						filterWindow.DataContext = new FilterListWindowViewModel(filters);
					}
					else {
						filters = new ObservableCollection<FilterDescription>();
						filterDictionary.Add(this._watchingFolder.WatchPath, filters);
						filterWindow.DataContext = new FilterListWindowViewModel(filters);
					}

					if (filterWindow.ShowDialog() == true) {

					}

				}
			, null);
		}

		public ICommand DeleteCommand { get; private set; }
		public ICommand ModifyFilterCommand { get; private set; }

	}

	public class CompareSelector<T, TKey> : IEqualityComparer<T> {
		private Func<T, TKey> selector;

		public CompareSelector(Func<T, TKey> selector) {
			this.selector = selector;
		}

		public bool Equals(T x, T y) {
			return selector(x).Equals(selector(y));
		}

		public int GetHashCode(T obj) {
			return selector(obj).GetHashCode();
		}
	} 


	public class FolderSettingListViewModel : ViewModelBase ,IDisposable{
		public ObservableCollection<FolderSettingViewModel> WatchingFolders { get; private set; }


		public FolderSettingListViewModel() {
			WatchingFolders = new ObservableCollection<FolderSettingViewModel>();
		}

		public void AddFolder(string path, bool isSubDirectory,
			bool isCreate,
			bool isDelete,
			bool isChange,
			bool isRename) {
			var component = Model.WatcherComponent.Instance;
			WatchingFolderDescription fd = new WatchingFolderDescription(component.EventNotifiter);
			fd.WatchPath = path;
			fd.IsSubDirectory = isSubDirectory;
			fd.IsNotifyCreate = isCreate;
			fd.IsNotifyDelete = isDelete;
			fd.IsNotifyChange = isChange;
			fd.IsNotifyRename = isRename;


			var fsvm = new FolderSettingViewModel(fd, WatchingFolders);

			if (WatchingFolders.Contains(fsvm,
				new CompareSelector<FolderSettingViewModel, string>(f => f.WatchPath))
				) {
					System.Windows.MessageBox.Show("もうある");
				return;
			}


			WatchingFolders.Add(fsvm);

		}

		public void Dispose() {

		}
	}
}
