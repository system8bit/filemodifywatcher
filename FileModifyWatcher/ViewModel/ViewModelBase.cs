﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ComponentModel;

namespace Tools.FileModifyWatcher.ViewModel {
	public class ViewModelBase :INotifyPropertyChanged {
		public event PropertyChangedEventHandler PropertyChanged;

		protected void NotifyPropertyChanged(params string[] names) {
			if (this.PropertyChanged != null) {
				foreach (string name in names) {
					this.PropertyChanged(this, new PropertyChangedEventArgs(name));
				}
			}
		}
	}
}
