﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tools.FileModifyWatcher.ViewModel {
	public class FolderSettingTabViewModel : ViewModelBase ,IDisposable {
		private FolderSettingAddViewModel _folderSettingAddViewModel;

		public FolderSettingAddViewModel FolderSettingAddViewModel {
			get { return _folderSettingAddViewModel; }
		}
		private FolderSettingListViewModel _folderSettingViewModel;

		public FolderSettingListViewModel FolderSettingListViewModel {
			get { return _folderSettingViewModel; }
		}


		public FolderSettingTabViewModel() {
			_folderSettingViewModel = new FolderSettingListViewModel();
			_folderSettingAddViewModel = new FolderSettingAddViewModel(_folderSettingViewModel);
		}

		public void Dispose() {
			if (_folderSettingAddViewModel != null) {
				_folderSettingAddViewModel.Dispose();
				_folderSettingAddViewModel = null;
			}

			if (_folderSettingViewModel != null) {
				_folderSettingViewModel.Dispose();
				_folderSettingViewModel = null;
			}
		}
	}
}
