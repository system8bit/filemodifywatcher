﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;

namespace Tools.FileModifyWatcher.ViewModel {
	class MainWindowViewModel : ViewModelBase,IDisposable{
		private FolderSettingTabViewModel _folderSettingTabViewModel = null;

		public FolderSettingTabViewModel FolderSettingTabViewModel {
			get { return _folderSettingTabViewModel; }
		}

		private Window _owner;


		public MainWindowViewModel(Window owner) {
			this._owner = owner;
			_folderSettingTabViewModel = new FolderSettingTabViewModel();

		}

		public void Dispose() {
			if (_folderSettingTabViewModel != null) {
				_folderSettingTabViewModel.Dispose();
				_folderSettingTabViewModel = null;
			}

		}
	}
}
