﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections.ObjectModel;

using System.Windows.Data;
using System.Windows.Input;

using Tools.FileModifyWatcher.Command;


using Tools.FileModifyWatcher.Model;

namespace Tools.FileModifyWatcher.ViewModel {
	public class FilterViewModel : ViewModelBase{
		public int ProcessPriority {
			get {
				return _filter.ProcessPriority;
			}
			set {
				_filter.ProcessPriority = value;
			}
		}

		public MatchPart MatchPart {
			get {
				return _filter.MatchPart;
			}
			set {
				_filter.MatchPart = value;
			}
		}

		public string Keyword {
			get {
				return _filter.Keyword;
			}
			set {
				_filter.Keyword = value;
			}
		}
		public MatchMethod MatchMethod {
			get {
				return _filter.MatchMethod;
			}
			set {
				_filter.MatchMethod = value;
			}
		}

		public MatchAction MatchAction {
			get {
				return _filter.MatchAction;
			}
			set {
				_filter.MatchAction = value;
			}
		}

		public string Option {
			get {
				return _filter.Option;
			}
			set {
				_filter.Option = value;
			}
		}

		public bool IsChain {
			get {
				return _filter.IsChain;
			}
			set {
				_filter.IsChain = value;
			}
		}

		public ICommand DeleteFromListCommand { get; private set; }

		public FilterViewModel(ObservableCollection<FilterDescription> list) {
			_list = list;

			DeleteFromListCommand = new DelegateCommand(
				() => {
					//_list.Remove(this);
				}
			);
		}

		ObservableCollection<FilterDescription> _list;

		Model.FilterDescription _filter;

		public FilterViewModel(Model.FilterDescription filter) {
			this._filter = filter;
		}
	}

	public class FilterListViewModel : ViewModelBase {
		public ObservableCollection<FilterDescription> Filters { get; private set; }

		public FilterListViewModel(ObservableCollection<FilterDescription> filters) {
			Filters = filters;
		}

		public void AddFilter(FilterDescription fvm) {
			fvm.ProcessPriority = Filters.Count + 1;
			Filters.Add(fvm);
		}

	}
}
