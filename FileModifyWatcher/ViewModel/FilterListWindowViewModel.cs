﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Input;
using Tools.FileModifyWatcher.Command;

using Tools.FileModifyWatcher.Model;

using System.Collections.ObjectModel;

namespace Tools.FileModifyWatcher.ViewModel {
	public class FilterListWindowViewModel : ViewModelBase {
		public FilterListViewModel FilterListViewModel { get; private set; }

		public FilterListWindowViewModel(ObservableCollection<FilterDescription> filters) {
			FilterListViewModel = new FilterListViewModel(filters);
			InitializeCommand();
		}

		public ICommand AddFilterCommand { get; private set; }
		void InitializeCommand() {
			AddFilterCommand = new DelegateCommand(
				() => {
					var fvm = new FilterDescription();
					fvm.Keyword = string.Empty;
					fvm.IsChain = false;
					fvm.MatchAction = MatchAction.StoreDatabase;
					fvm.MatchPart = MatchPart.FullPath;
					fvm.MatchMethod = MatchMethod.PartMatch;
					fvm.Option = string.Empty;
					FilterListViewModel.AddFilter(fvm);
				});

		}
	}
}
