﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Data;

using Tools.FileModifyWatcher.Model;

namespace Tools.FileModifyWatcher.ViewModel {
	[ValueConversion(typeof(bool),typeof(string))]
	public class SubDirectoryCheckBoxConverter : IValueConverter {
		const string watching = "対象";
		const string notWatching = "監視しない";


		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			bool enable = (bool)value;

			return enable ? watching : notWatching;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			string s = (string)value;

			return s == watching ? true : false;

		}
	}



	[ValueConversion(typeof(MatchPart), typeof(string))]
	public class MatchPartConverter : IValueConverter {
		const string fileName = "マッチ対象の名前";
		const string fullPath = "マッチ対象のフルパス";
		const string upperDirectoryName = "上位ディレクトリの名前";
		const string upperDirectoryPath = "上位ディレクトリのパス";


		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			MatchPart e = (MatchPart)value;

			string ret = string.Empty;

			switch (e) {
				case MatchPart.FileName:
					ret = fileName;
					break;
				case MatchPart.FullPath:
					ret = fullPath;
					break;
				case MatchPart.UpperDirectoryName:
					ret = upperDirectoryName;
					break;
				case MatchPart.UpperDirectoryPath:
					ret = upperDirectoryPath;
					break;
			}


			return ret;
		}
		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			string s = (string)value;

			MatchPart ret = MatchPart.FullPath;

			switch (s) {
				case fileName:
					ret = MatchPart.FileName;
					break;

				case fullPath:
					ret = MatchPart.FullPath;
					break;
				case upperDirectoryName:
					ret = MatchPart.UpperDirectoryName;
					break;
				case upperDirectoryPath:
					ret = MatchPart.UpperDirectoryPath;
					break;
			}
			return ret;
		}
	}

	[ValueConversion(typeof(MatchPart[]), typeof(string[]))]
	public class MatchPartEnumConverter : IValueConverter {
		const string fileName = "マッチ対象の名前";
		const string fullPath = "マッチ対象のフルパス";
		const string upperDirectoryName = "上位ディレクトリの名前";
		const string upperDirectoryPath = "上位ディレクトリのパス";


		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			MatchPart[] e = (MatchPart[])value;

			string[] ret = new string[e.Length];

			for (int i = 0; i < e.Length; i++) {
				switch (e[i]) {
					case MatchPart.FileName:
						ret[i] = fileName;
						break;
					case MatchPart.FullPath:
						ret[i] = fullPath;
						break;
					case MatchPart.UpperDirectoryName:
						ret[i] = upperDirectoryName;
						break;
					case MatchPart.UpperDirectoryPath:
						ret[i] = upperDirectoryPath;
						break;
				}
			}

			return ret;
		}
		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			string[] s = (string[])value;

			MatchPart[] ret = new MatchPart[s.Length];

			for (int i = 0; i < s.Length; i++) {
				switch (s[i]) {
					case fileName:
						ret[i] = MatchPart.FileName;
						break;
					case fullPath:
						ret[i] = MatchPart.FullPath;
						break;
					case upperDirectoryName:
						ret[i] = MatchPart.UpperDirectoryName;
						break;
					case upperDirectoryPath:
						ret[i] = MatchPart.UpperDirectoryPath;
						break;
				}
			}

			return ret;
		}
	}


	[ValueConversion(typeof(MatchMethod), typeof(string))]
	public class MatchMethodConverter : IValueConverter {
		const string partMatch = "部分一致";
		const string completeMatch = "完全一致";
		const string regexMatch = "正規表現で一致";


		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			MatchMethod e = (MatchMethod)value;

			string ret = string.Empty;

			switch (e) {
				case MatchMethod.PartMatch:
					ret = partMatch;
					break;
				case MatchMethod.CompleteMatch:
					ret = completeMatch;
					break;
				case MatchMethod.RegexMatch:
					ret = regexMatch;
					break;
			}

			return ret;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			string s = (string)value;

			MatchMethod ret = MatchMethod.PartMatch;

			switch (s) {
				case partMatch:
					ret = MatchMethod.PartMatch;
					break;
				case completeMatch:
					ret = MatchMethod.CompleteMatch;
					break;
				case regexMatch:
					ret = MatchMethod.RegexMatch;
					break;
			}
			return ret;
		}
	}


	[ValueConversion(typeof(MatchMethod[]), typeof(string[]))]
	public class MatchMethodEnumConverter : IValueConverter {
		const string partMatch = "部分一致";
		const string completeMatch = "完全一致";
		const string regexMatch = "正規表現で一致";


		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			MatchMethod[] e = (MatchMethod[])value;

			string[] ret = new string[e.Length];

			for (int i = 0; i < e.Length; i++) {
				switch (e[i]) {
					case MatchMethod.PartMatch:
						ret[i] = partMatch;
						break;
					case MatchMethod.CompleteMatch:
						ret[i] = completeMatch;
						break;
					case MatchMethod.RegexMatch:
						ret[i] = regexMatch;
						break;
				}
			}

			return ret;
		}
		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			string[] s = (string[])value;

			MatchMethod[] ret = new MatchMethod[s.Length];

			for (int i = 0; i < s.Length; i++) {
				switch (s[i]) {
					case partMatch:
						ret[i] = MatchMethod.PartMatch;
						break;
					case completeMatch:
						ret[i] = MatchMethod.CompleteMatch;
						break;
					case regexMatch:
						ret[i] = MatchMethod.RegexMatch;
						break;
				}
			}

			return ret;
		}
	}

	[ValueConversion(typeof(MatchAction), typeof(string))]
	public class MatchActionConverter : IValueConverter {
		const string storeDatabase = "データベースにログを保存";
		const string launchApplication = "プログラムを起動";
		const string popUpNotifiy = "ポップアップ表示";
		const string nothing = "何もしない";


		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			MatchAction e = (MatchAction)value;

			string ret = string.Empty;

			switch (e) {
				case MatchAction.StoreDatabase:
					ret = storeDatabase;
					break;
				case MatchAction.LaunchApplication:
					ret = launchApplication;
					break;
				case MatchAction.PopUpNotifiy:
					ret = popUpNotifiy;
					break;
				case MatchAction.Nothing:
					ret = nothing;
					break;
			}

			return ret;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			string s = (string)value;

			MatchAction ret = MatchAction.Nothing;

			switch (s) {
				case storeDatabase:
					ret = MatchAction.StoreDatabase;
					break;

				case launchApplication:
					ret = MatchAction.LaunchApplication;
					break;
				case popUpNotifiy:
					ret = MatchAction.PopUpNotifiy;
					break;
				case nothing:
					ret = MatchAction.Nothing;
					break;
			}
			return ret;
		}
	}


	[ValueConversion(typeof(MatchAction[]), typeof(string[]))]
	public class MatchActionEnumConverter : IValueConverter {
		const string storeDatabase = "データベースにログを保存";
		const string launchApplication = "プログラムを起動";
		const string popUpNotifiy = "ポップアップ表示";
		const string nothing = "何もしない";


		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			MatchAction[] e = (MatchAction[])value;

			string[] ret = new string[e.Length];

			for (int i = 0; i < e.Length; i++) {
				switch (e[i]) {
					case MatchAction.StoreDatabase:
						ret[i] = storeDatabase;
						break;
					case MatchAction.LaunchApplication:
						ret[i] = launchApplication;
						break;
					case MatchAction.PopUpNotifiy:
						ret[i] = popUpNotifiy;
						break;
					case MatchAction.Nothing:
						ret[i] = nothing;
						break;
				}
			}

			return ret;
		}
		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			string[] s = (string[])value;

			MatchAction[] ret = new MatchAction[s.Length];

			for (int i = 0; i < s.Length; i++) {
				switch (s[i]) {
					case storeDatabase:
						ret[i] = MatchAction.StoreDatabase;
						break;
					case launchApplication:
						ret[i] = MatchAction.LaunchApplication;
						break;
					case popUpNotifiy:
						ret[i] = MatchAction.PopUpNotifiy;
						break;
					case nothing:
						ret[i] = MatchAction.Nothing;
						break;
				}
			}

			return ret;
		}
	}

}
