﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Input;

using Ookii.Dialogs.Wpf;

using Tools.FileModifyWatcher.Command;
using Tools.FileModifyWatcher.Model;


namespace Tools.FileModifyWatcher.ViewModel {
	public class FolderSettingAddViewModel : ViewModelBase ,IDisposable{
		FolderSettingListViewModel _folderSettingList;

		public ICommand FolderReferenceCommand { get; private set; }
		public ICommand FolderAddCommand { get; private set; }

		public string WatchingFolderText { get; set; }
		public bool IsSubDirectory { get; set; }
		public bool IsNotifyCreate { get; set; }
		public bool IsNotifyDelete { get; set; }
		public bool IsNotifyChange { get; set; }
		public bool IsNotifyRename { get; set; }


		public FolderSettingAddViewModel(FolderSettingListViewModel folderSettingList) {
			InitializeCommand();

			this._folderSettingList = folderSettingList;
		}

		private void clearInputData() {
			WatchingFolderText = string.Empty;
			IsSubDirectory = false;
			IsNotifyCreate = false;
			IsNotifyDelete = false;
			IsNotifyChange = false;
			IsNotifyRename = false;
		}

		private void InitializeCommand() {
			FolderReferenceCommand = new DelegateCommand(
				() => {
					VistaFolderBrowserDialog vfbd = new VistaFolderBrowserDialog();
					if (vfbd.ShowDialog() == true) {
						this.WatchingFolderText = vfbd.SelectedPath;
						NotifyPropertyChanged("WatchingFolderText");
					}
				}, 
				null);

			FolderAddCommand = new DelegateCommand(
				() => {
					_folderSettingList.AddFolder(
						this.WatchingFolderText,
						this.IsSubDirectory,
						this.IsNotifyCreate,
						this.IsNotifyDelete,
						this.IsNotifyChange,
						this.IsNotifyRename
						);
					clearInputData();
					NotifyPropertyChanged("WatchingFolderText", "IsSubDirectory"
						, "IsNotifyCreate"
						, "IsNotifyDelete"
						, "IsNotifyChange"
						, "IsNotifyRename"
						);
				},
				null);
		}



		public void Dispose() {
			
		}
	}
}
