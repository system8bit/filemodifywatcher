﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tools.FileModifyWatcher.ViewModel {
	public enum DataViewLimit :int {
		Count100 = 100 ,
		Count300 = 300,
		Count500 = 500,
		Count700 = 700,
		Count1000 = 1000,
		Count2000 = 2000,
		CountInfinity = Int32.MaxValue,
	}
	class ModifyLogViewModel : ViewModelBase {

	}
}
