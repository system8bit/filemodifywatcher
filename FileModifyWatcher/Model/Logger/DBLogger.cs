﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Data.SqlServerCe;

namespace Tools.FileModifyWatcher.Model.Logger {
	class DBLogger {
		private SqlCeConnection connection;
		private bool isOpen = false;

		private string dbName = "log.sdf";
		private string connectionString = @"Data Source=log.sdf";
		static private string tableCreateCommand = "CREATE TABLE WATCH_LOG(" +
			"LOG_ID BIGINT PRIMARY KEY IDENTITY(1,1) NOT NULL" +
			",CHANGE_TYPE NVARCHAR(256) NOT NULL" +
			",FILE_PATH NVARCHAR(256) NOT NULL" +
			",LAST_WRITE_TIME DATETIME NOT NULL" +
			",LAST_ACCESS_TIME DATETIME NOT NULL" +
			",DATA_TYPE NVARCHAR(10) NOT NULL" +
			",OLD_PATH NVARCHAR(256) NOT NULL" +
			",WATCHER_PATH NVARCHAR(256) NOT NULL" +
			",CREATE_TIME DATETIME NOT NULL" +
			",EXIST TINYINT NOT NULL" +
			")";


		static private string DataPostCommand(WatchLogData log) {
				string changeTypeStr = System.Enum.GetName(typeof(DirectoryOrFile), log.ChangeType);
			
				string name = string.Empty;
			string sqlString = "INSERT INTO WATCH_LOG " +
										"(CHANGE_TYPE" +
										",FILE_NAME" +
										",LAST_WRITE_TIME" +
										",LAST_ACCESS_TIME" +
										",DATA_TYPE" +
										",OLD_PATH" +
										",WATCHER_PATH" +
										",CREATE_TIME" +
										",EXIST" +
										") VALUES (" +
										"'" + log.CurrentPathInfo.ToString() +
										"', '" + name.ToString() +
										"', '" + name.ToString() +
										"', '" + log.LastWriteDate.ToString() +
										"', '" + name.ToString() +
										"', '" + name.ToString() +
										"', '" + name.ToString() +
										"')";

			return sqlString;
		}

	

		static private string DataGetCommand() {
			string sqlString = string.Empty;
			return sqlString;
		}

		private DBLogger() {
			InitializeDB();
		}

		static private DBLogger instance = null;
		static public DBLogger Instance {
			get {
				if (instance == null) {
					instance = new DBLogger();
				}

				return instance;
			}
		}

		public void InitializeDB() {

			if (File.Exists(dbName)) {
				return;
			}

			SqlCeEngine engine = new SqlCeEngine(connectionString);

			engine.CreateDatabase();
			engine.Dispose();

			using (SqlCeConnection connection = new SqlCeConnection(connectionString)) {
				connection.Open();

				SqlCeCommand command = new SqlCeCommand(tableCreateCommand, connection);
				command.CommandType = System.Data.CommandType.Text;
				try {
					command.ExecuteNonQuery();
				}
				catch (Exception e) {

					System.Diagnostics.Debug.WriteLine(e);
				}

				connection.Close();
			}

		}

		public void SqlOpen() {
			if (!isOpen) {
				connection = new SqlCeConnection(connectionString);
				connection.Open();

				isOpen = true;
			}
		}

		public void SqlClose() {
			if (isOpen) {
				connection.Close();
				isOpen = false;
			}
		}

		public void StoreLog(WatchLogData log) {
			SqlOpen();

			var sqlString = DataPostCommand(log);
			SqlCeCommand command = new SqlCeCommand(sqlString, connection);

			try {
				command.ExecuteNonQuery();
			}
			catch (Exception) {
			}
		}

		public void GetLog() {
			SqlOpen();

			string sqlString = string.Empty;

			SqlCeCommand command = new SqlCeCommand(sqlString, connection);

			var reader = command.ExecuteReader();

			while (reader.Read()) {


			}

		}

	}
}
