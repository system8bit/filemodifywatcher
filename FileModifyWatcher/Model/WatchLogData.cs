﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace Tools.FileModifyWatcher.Model {
	public enum DirectoryOrFile{
		Unknowon,
		Directory,
		File,
	}

	public class PathInfo {
		DirectoryOrFile _type;

		public PathInfo(string fullPath) {
			this._fullPath = fullPath;

			if (Directory.Exists(this._fullPath)) {
				this._type = DirectoryOrFile.Directory;
			}
			else if (File.Exists(this._fullPath)) {
				this._type = DirectoryOrFile.File;
			}
			else {
				this._type = DirectoryOrFile.Unknowon;
			}
		}

		public DirectoryOrFile Type {
			get { return _type; }
			set { _type = value; }
		}

		string _fullPath;

		public string FullPath {
			get { return _fullPath; }
		}

		public string UpplerFolderPath {
			get {
				try{
					return Path.GetDirectoryName(_fullPath);
				}
				catch{
					return string.Empty;
				}
			}
		}

		public string Ext {
			get {
				if (_type == DirectoryOrFile.File) {
					return Path.GetExtension(this._fullPath);
				}
				return string.Empty;
			}
		}

		public string Name {
			get {
				return Path.GetFileName(this._fullPath);
			}
		}

		public string NameWithoutExt{
			get {
				if (_type == DirectoryOrFile.File) {
					return Path.GetFileNameWithoutExtension(this._fullPath);
				}
				return string.Empty;
			}
		}
	}

	public class WatchLogData {
		private PathInfo _currentPathInfo = null;

		public PathInfo CurrentPathInfo {
			get { return _currentPathInfo; }
			set { _currentPathInfo = value; }
		}
		private PathInfo _oldPathInfo = null;

		public PathInfo OldPathInfo {
			get { return _oldPathInfo; }
			set { _oldPathInfo = value; }
		}

		private WatcherChangeTypes _changeType = WatcherChangeTypes.All;

		public WatcherChangeTypes ChangeType {
			get { return _changeType; }
		}

		private string _watcherPath = string.Empty;

		public string WatcherPath {
			get { return _watcherPath; }
		}

		private DirectoryOrFile _directroyOrFile = DirectoryOrFile.Unknowon;

		public DirectoryOrFile DirectroyOrFile {
			get { return _directroyOrFile; }
		}

		private DateTime _lastAccessDate;

		public DateTime LastAccessDate {
			get { return _lastAccessDate; }
		}

		private DateTime _lastWriteDate;

		public DateTime LastWriteDate {
			get { return _lastWriteDate; }
		}

		private DateTime _createDate;

		public DateTime CreateDate {
			get { return _createDate; }
			set { _createDate = value; }
		}

		private bool _exist;

		public bool Exist {
			get { return _exist; }
			set { _exist = value; }
		}

		private WatchLogData() { }// Use Create Method

		static public WatchLogData Create(FileSystemWatcher watcher,FileSystemEventArgs fsea){
			WatchLogData log = new WatchLogData();

			if (watcher != null) {
				log._watcherPath = watcher.Path;
			}
			

			log._changeType = fsea.ChangeType;

			RenamedEventArgs rea = fsea as RenamedEventArgs;
			if (rea != null) {
				log._oldPathInfo = new PathInfo(rea.OldFullPath);
			}
			else {
				log._oldPathInfo = null;
			}

			FileSystemInfo detailInfo;
			log._currentPathInfo = new PathInfo(fsea.FullPath);
			if (log._currentPathInfo.Type == DirectoryOrFile.Directory) {
				detailInfo = new DirectoryInfo(log._currentPathInfo.FullPath);
			}
			else if (log._currentPathInfo.Type == DirectoryOrFile.File) {
				detailInfo = new FileInfo(log._currentPathInfo.FullPath);
			}
			else {
				detailInfo = new FileInfo(log._currentPathInfo.FullPath);//process as file
			}

			log._createDate = detailInfo.CreationTime;
			log._lastAccessDate = detailInfo.LastAccessTime;
			log._lastWriteDate = detailInfo.LastWriteTime;

			log._exist = log.Exist;

			


			return log;
		}
	}
}
