﻿using System;
using System.IO;

namespace Tools.FileModifyWatcher.Model.Watcher {
	public class WatcherHelper {
		public delegate void FileWatchEventHandler(object sender, FileSystemEventArgs fsea);
	}

	public interface IWatcher {
		event WatcherHelper.FileWatchEventHandler FileWatch;
		string Filter { get; set; }
		bool IncludeSubdirectories { get; set; }
		System.IO.NotifyFilters NotifyFilter { get; set; }
		void StartWatch();
		void StopWatch();
		string WatchPath { get; set; }

		bool isWatching { get; }
	}
}
