﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;


namespace Tools.FileModifyWatcher.Model.Watcher {
	public class APIWatcher : Tools.FileModifyWatcher.Model.Watcher.IWatcher {
		FileSystemWatcher watcher;

		public APIWatcher(string watchPath, bool watchSubDirectory) {
			watcher = new FileSystemWatcher(watchPath);
			watcher.IncludeSubdirectories = watchSubDirectory;

			watcher.Changed += new FileSystemEventHandler(watchEvent);
			watcher.Created += new FileSystemEventHandler(watchEvent);
			watcher.Deleted += new FileSystemEventHandler(watchEvent);
			watcher.Renamed += new RenamedEventHandler(watchEvent);

			watcher.Error += new ErrorEventHandler(watcher_Error);

		}

		public event WatcherHelper.FileWatchEventHandler FileWatch;

		void watchEvent(object sender, FileSystemEventArgs fsea) {
			object o = this;
			if (FileWatch != null) {
				FileWatch(o, fsea);
			}
		}

		void watcher_Error(object sender, ErrorEventArgs e) {
			watcher.InternalBufferSize = watcher.InternalBufferSize * 2 + 1;			

		}

		public bool IncludeSubdirectories {
			get {
				return watcher.IncludeSubdirectories;
			}
			set {
				watcher.IncludeSubdirectories = value;
			}
		}

		public string Filter {
			get {
				return watcher.Filter;
			}
			set {
				watcher.Filter = value;
			}
		}

		public string WatchPath {
			get {
				return watcher.Path;
			}
			set {
				watcher.Path = value;
			}
		}

		public NotifyFilters NotifyFilter {
			get {
				return watcher.NotifyFilter;
			}
			set {
				watcher.NotifyFilter = value;
			}
		}

		public int InternalBufferSize {
			get {
				return watcher.InternalBufferSize;
			}
			set {
				watcher.InternalBufferSize = value;
			}
		}

		public void StartWatch() {
			watcher.EnableRaisingEvents = true;

		}

		public void StopWatch() {
			watcher.EnableRaisingEvents = false;

		}

		public void Dispose() {
			if (watcher != null) {
				watcher.EnableRaisingEvents = false;
				watcher.Dispose();
				watcher = null;
			}
		}


		public bool isWatching {
			get {
				return watcher.EnableRaisingEvents;
			}
		}
	}
}
