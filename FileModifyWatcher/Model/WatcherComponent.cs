﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Collections.ObjectModel;

namespace Tools.FileModifyWatcher.Model {
	public class WatchingFolderDescription {
		public bool IsEnable {
			get {
				return watcher.EnableRaisingEvents;
			}
			set {
				watcher.EnableRaisingEvents = value;
			}
		}
		public string WatchPath {
			get {
				return watcher.Path;
			}
			set {
				watcher.Path = value;
			}
		}

		public bool IsSubDirectory {
			get {
				return watcher.IncludeSubdirectories;
			}
			set {
				watcher.IncludeSubdirectories = value;
			}
		}

		private bool isCreate = false;
		public bool IsNotifyCreate {
			get {
				return isCreate;
			}
			set {
				isCreate = value;
				if (isCreate) {
					watcher.Created += new FileSystemEventHandler(this._notifier.FileChangeNofityEventReceiver);
				}
				else {
					watcher.Created -= new FileSystemEventHandler(this._notifier.FileChangeNofityEventReceiver);
				}
			}
		}

		private bool isDelete = false;
		public bool IsNotifyDelete {
			get {
				return isDelete;
			}
			set {
				isDelete = value;
				if (isDelete) {
					watcher.Deleted += new FileSystemEventHandler(this._notifier.FileChangeNofityEventReceiver);
				}
				else {
					watcher.Deleted -= new FileSystemEventHandler(this._notifier.FileChangeNofityEventReceiver);
				}
			}
		}

		private bool isRename = false;
		public bool IsNotifyRename {
			get {
				return isRename;
			}
			set {
				isRename = value;
				if (isRename) {
					watcher.Renamed += new RenamedEventHandler(this._notifier.FileRenameNofityEventReceiver);
				}
				else {
					watcher.Renamed -= new RenamedEventHandler(this._notifier.FileRenameNofityEventReceiver);
				}
			}
		}

		private bool isChange = false;
		public bool IsNotifyChange {
			get {
				return isChange;
			}
			set {
				isChange = value;
				if (isChange) {
					watcher.Changed += new FileSystemEventHandler(this._notifier.FileChangeNofityEventReceiver);
				}
				else {
					watcher.Changed -= new FileSystemEventHandler(this._notifier.FileChangeNofityEventReceiver);
				}
			}
		}



		private FileSystemWatcher watcher;

		public WatchingFolderDescription(FileChangeEventNotifier notifier) {
			watcher = new FileSystemWatcher();
			watcher.NotifyFilter |=
				NotifyFilters.Attributes
				| NotifyFilters.CreationTime
				| NotifyFilters.DirectoryName
				| NotifyFilters.FileName
				| NotifyFilters.LastAccess
				| NotifyFilters.LastWrite
				| NotifyFilters.Security
				| NotifyFilters.Size;

			watcher.Error += new ErrorEventHandler(watcher_Error);
			this._notifier = notifier;
		}

		void watcher_Error(object sender, ErrorEventArgs e) {
			watcher.InternalBufferSize = watcher.InternalBufferSize * 2 + 1;
		}

		FileChangeEventNotifier _notifier;
	}

	public class FileChangeEventNotifier {
		public void FileChangeNofityEventReceiver(object sender, FileSystemEventArgs e) {
			var watcher = sender as FileSystemWatcher;
			WatchLogData log = WatchLogData.Create(watcher, e);

			var watcherDictionary = WatcherComponent.Instance.FilterDictionary;
			ObservableCollection<FilterDescription> filters;
			if (watcherDictionary.TryGetValue(log.WatcherPath, out filters)) {
				foreach (var f in filters) {
					if (!f.Process(log)) {
						break;
					}
				}
			}
		}

		public void FileRenameNofityEventReceiver(object sender, RenamedEventArgs e) {
			FileChangeNofityEventReceiver(sender, e);
		}
	}

	public class WatcherComponent {
		private WatcherComponent() {
		}

		static WatcherComponent _instance;

		void Initialize() {
			_instance = new WatcherComponent();
			_instance._eventNotifiter = new FileChangeEventNotifier();
			_instance._filterDictionary = new Dictionary<string, ObservableCollection<FilterDescription>>();
		}

		static public WatcherComponent Instance {
			get {
				if (_instance == null) {
					_instance = new WatcherComponent();
					_instance.Initialize();
				}
				return _instance;
			}
		}

		FileChangeEventNotifier _eventNotifiter;

		public FileChangeEventNotifier EventNotifiter {
			get { return _eventNotifiter; }
		}

		Dictionary<string, ObservableCollection<FilterDescription>> _filterDictionary;

		public Dictionary<string, ObservableCollection<FilterDescription>> FilterDictionary {
			get { return _filterDictionary; }
			set { _filterDictionary = value; }
		}

	}

	public enum MatchPart {
		FullPath,
		FileName,
		UpperDirectoryName,
		UpperDirectoryPath,
	}

	public enum MatchMethod {
		PartMatch,
		CompleteMatch,
		RegexMatch,
	}


	public enum MatchAction {
		StoreDatabase,
		LaunchApplication,
		PopUpNotifiy,
		Nothing,
	}

	public class FilterDescription {
		private int _processPriority;

		public int ProcessPriority {
			get { return _processPriority; }
			set { _processPriority = value; }
		}


		private MatchPart _matchPart;

		public MatchPart MatchPart {
			get { return _matchPart; }
			set { 
				_matchPart = value;
				if (_matchPart == Model.MatchPart.FileName) {
					_filter.Part = Filter.LogPart.NameOnly;
				}
				else if (_matchPart == Model.MatchPart.FullPath) {
					_filter.Part = Filter.LogPart.FullPath;
				}
				else if (_matchPart == Model.MatchPart.UpperDirectoryName) {
					_filter.Part = Filter.LogPart.UpperDirectory;
				}
				else if (_matchPart == Model.MatchPart.UpperDirectoryPath) {
					_filter.Part = Filter.LogPart.FullPath;//
				}

			}
		}

		private string _keyword;

		public string Keyword {
			get { return _keyword; }
			set { _keyword = value; }
		}

		private MatchMethod _matchMethod;

		public MatchMethod MatchMethod {
			get { return _matchMethod; }
			set { 
				_matchMethod = value;
				if (_matchMethod == Model.MatchMethod.PartMatch) {
					_filter = new Filter.MatchFilter(_keyword, false, false);
				}
				else if (_matchMethod == Model.MatchMethod.CompleteMatch) {
					_filter = new Filter.MatchFilter(_keyword, false, true);
				}
				else if (_matchMethod == Model.MatchMethod.RegexMatch) {
					_filter = new Filter.RegexFilter(_keyword);
				}

			}
		}

		private MatchAction _matchAction;

		public MatchAction MatchAction {
			get { return _matchAction; }
			set { 
				_matchAction = value;
				if (_matchAction == Model.MatchAction.StoreDatabase) {
					_action = new Action.DBLogAction();
				}
				else if (_matchAction == Model.MatchAction.LaunchApplication) {
					_action = new Action.ProgramLaunchAction(this._option);
				}
				else if (_matchAction == Model.MatchAction.PopUpNotifiy) {
					_action = new Action.DoNothingAction();//
				}
				else if (_matchAction == Model.MatchAction.Nothing) {
					_action = new Action.DoNothingAction();
				}
			}
		}

		private string _option;

		public string Option {
			get { return _option; }
			set { 
				_option = value;
				var launch = _action as Action.ProgramLaunchAction;
				if (launch != null) {
					launch.SetCommand(_option);
				}
			}
		}

		private bool _isChain;

		public bool IsChain {
			get { return _isChain; }
			set { _isChain = value; }
		}

		private Model.Action.IAction _action = new Action.DBLogAction();

		private Model.Filter.IFilter _filter = new Filter.MatchFilter(string.Empty, false, false);

		/// <summary>
		/// 次のフィルタも実行するならtrueがかえる
		/// </summary>
		/// <param name="log"></param>
		/// <returns></returns>
		public bool Process(WatchLogData log) {
			if (_filter.Check(log)) {
				_action.Do(log);
				return !_isChain;
			}
			return true;
		}
	}
}