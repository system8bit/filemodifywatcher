﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tools.FileModifyWatcher.Model {
	public class WatcherFatalException : Exception {

	}

	public class WatcherWarningException : Exception {

	}

	public class WatcherInfomationException : Exception {

	}
}
