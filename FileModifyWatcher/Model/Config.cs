﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tools.FileModifyWatcher.Model {
	public class Config {
		[System.Xml.Serialization.XmlIgnoreAttribute]
		static string _filename = @"config.xml";

		[System.Xml.Serialization.XmlIgnoreAttribute]
		static Config _instance = null;

		public static Config Instance {
			get {
				if (_instance == null) {
					_instance = Reload();
				} 
				return Config._instance;
			}
		}

		private Config() { }

		private static Config MakeDefault() {
			Config config = new Config();

			return config;
		}

		public class WatcherConfig {
			private string watchPath = string.Empty;

			public string WatchPath {
				get { return watchPath; }
				set { watchPath = value; }
			}
			private bool watchSubDirectory = false;

			public bool WatchSubDirectory {
				get { return watchSubDirectory; }
				set { watchSubDirectory = value; }
			}
		}

		List<WatcherConfig> watcherConfigList = new List<WatcherConfig>();

		public List<WatcherConfig> WatcherConfigList {
			get { return watcherConfigList; }
			set { watcherConfigList = value; }
		}

		private static Config Reload() {
			return Load(_filename);
		}

		private static Config Load(string filename) {
			System.Xml.Serialization.XmlSerializer serializer =
				new System.Xml.Serialization.XmlSerializer(typeof(FileModifyWatcher.Model.Config));

			Config config = null;

			try {
				System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Open);
				config = (Config)serializer.Deserialize(fs);

				fs.Close();
			}
			catch (System.IO.FileNotFoundException) {
				config = MakeDefault();
			}


			return config;
		}

		public static void Save(string filename) {
			System.Xml.Serialization.XmlSerializer serializer =
				new System.Xml.Serialization.XmlSerializer(typeof(FileModifyWatcher.Model.Config));

			System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create);

			serializer.Serialize(fs,_instance);

			fs.Close();
		}

		System.Windows.Size windowSize = new System.Windows.Size(300.0, 400.0);

		public System.Windows.Size WindowSize {
			get { return windowSize; }
			set { windowSize = value; }
		}

		System.Windows.Point startupPoint = new System.Windows.Point(0, 0);

		public System.Windows.Point StartupPoint {
			get { return startupPoint; }
			set { startupPoint = value; }
		}

		bool isEmptyConfig = true;

		public bool IsEmptyConfig {
			get { return isEmptyConfig; }
			set { isEmptyConfig = value; }
		}


	}
}
