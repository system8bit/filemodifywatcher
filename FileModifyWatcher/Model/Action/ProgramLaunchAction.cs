﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tools.FileModifyWatcher.Model.Action {
	public class ProgramLaunchAction : IAction{
		private string _command;
		private string _parameters;

		public ProgramLaunchAction(string launchPath) {
			SetCommand(launchPath);
		}

		public void SetCommand(string launchPath) {
			string[] divided = launchPath.Split(' ');
			string commandPath = divided[0];
			string parameters = string.Empty;
			if (divided.Length > 1) {
				for (int i = 1; i < divided.Length; i++) {
					parameters += divided[i] + " ";
				}
				parameters.Trim();
			}

			this._command = commandPath;
			this._parameters = parameters;
		}

		public void Do(WatchLogData log) {
			try {
				_parameters = _parameters.Replace(@"[% FULL_PATH %]", log.CurrentPathInfo.FullPath);
				_parameters = _parameters.Replace(@"[% NAME %]", log.CurrentPathInfo.Name);
				_parameters = _parameters.Replace(@"[% EXT %]", log.CurrentPathInfo.Ext);
				System.Diagnostics.Process.Start(_command, _parameters);
			}
			catch (System.IO.FileNotFoundException) {

			}
			catch (ObjectDisposedException) {

			}
			catch (System.ComponentModel.Win32Exception) {

			}
		}
	}
}
