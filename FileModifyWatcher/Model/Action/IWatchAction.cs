﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tools.FileModifyWatcher.Model.Action {
	public interface IAction {
		void Do(WatchLogData log);
	}
}
