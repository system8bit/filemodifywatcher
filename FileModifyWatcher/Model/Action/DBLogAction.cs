﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tools.FileModifyWatcher.Model;
using Tools.FileModifyWatcher.Model.Action;

namespace Tools.FileModifyWatcher.Model.Action {
	public class DBLogAction : IAction {
		public void Do(WatchLogData log) {
			var db = Logger.DBLogger.Instance;
			db.StoreLog(log);
		}
	}
}
