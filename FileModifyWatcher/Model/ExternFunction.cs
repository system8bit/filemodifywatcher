﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

using BOOL = System.Boolean;
using DWORD = System.UInt32;
using LPWSTR = System.String;
using NET_API_STATUS = System.UInt32;

namespace Tools.FileModifyWatcher.Model {
	using DWORD = System.UInt32;
	using PSECURITY_DESCRIPTOR = System.IntPtr;

	public class ExternFunction {
		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
		internal struct USE_INFO_2 {
			internal LPWSTR ui2_local;
			internal LPWSTR ui2_remote;
			internal LPWSTR ui2_password;
			internal DWORD ui2_status;
			internal DWORD ui2_asg_type;
			internal DWORD ui2_refcount;
			internal DWORD ui2_usecount;
			internal LPWSTR ui2_username;
			internal LPWSTR ui2_domainname;
		}

		[DllImport("NetApi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
		internal static extern NET_API_STATUS NetUseAdd(
			LPWSTR UncServerName,
			DWORD Level,
			ref USE_INFO_2 Buf,
			out DWORD ParmError);

		#region NetUseAdd sample
		/*
			USE_INFO_2 useInfo    = new USE_INFO_2();
			useInfo.ui2_local     = "E:";
			useInfo.ui2_remote    = @"\\machine\share";
			useInfo.ui2_password  = "password";
			useInfo.ui2_asg_type  = 0;    //disk drive
			useInfo.ui2_usecount  = 1;
			useInfo.ui2_username  = "user";
			useInfo.ui2_domainname= "domain";

			uint paramErrorIndex;
			uint returnCode = NetUseAdd(null, 2, ref useInfo, out paramErrorIndex);
			if (returnCode != 0)
			{
				  throw new System.ComponentModel.Win32Exception((int)returnCode);
			}
		 * */
		#endregion

	}
	
	public class Security {
		
		#region SECURITY_INFORMATION define in WinNT.h
		/*
		#define OWNER_SECURITY_INFORMATION       (0x00000001L)
		#define GROUP_SECURITY_INFORMATION       (0x00000002L)
		#define DACL_SECURITY_INFORMATION        (0x00000004L)
		#define SACL_SECURITY_INFORMATION        (0x00000008L)
		#define LABEL_SECURITY_INFORMATION       (0x00000010L)
		 * */
		#endregion
		enum SECURITY_INFORMATION : int {
			OWNER_SECURITY_INFORMATION = 1,
			GROUP_SECURITY_INFORMATION = 2,
			DACL_SECURITY_INFORMATION = 4,
			SACL_SECURITY_INFORMATION = 8,
			LABEL_SECURITY_INFORMATION = 10,
		}

		[StructLayoutAttribute(LayoutKind.Sequential)]
		struct SECURITY_DESCRIPTOR {
			public byte revision;
			public byte size;
			public short control;
			public IntPtr owner;
			public IntPtr group;
			public IntPtr sacl;
			public IntPtr dacl;
		}

		[DllImport("advapi32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		static extern bool GetFileSecurity(
		  string lpszUsername,
		  SECURITY_INFORMATION info,
		  PSECURITY_DESCRIPTOR descriptor,
		  DWORD val2,
		  out uint length);



		[DllImport("advapi32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		static extern bool SetFileSecurity(
			string lpszUsername,
			SECURITY_INFORMATION info, 
			PSECURITY_DESCRIPTOR descriptor);

		enum SID_NAME_USE {
			SidTypeUser = 1,
			SidTypeGroup,
			SidTypeDomain,
			SidTypeAlias,
			SidTypeWellKnownGroup,
			SidTypeDeletedAccount,
			SidTypeInvalid,
			SidTypeUnknown,
			SidTypeComputer
		}

		[DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		static extern bool LookupAccountSid(
			string lpSystemName,
			[MarshalAs(UnmanagedType.LPArray)] byte[] Sid,
			System.Text.StringBuilder lpName,
			ref uint cchName,
			System.Text.StringBuilder ReferencedDomainName,
			ref uint cchReferencedDomainName,
			out SID_NAME_USE peUse);    

		static public void GetFileSecurities(string filePath) {
			uint dwLength = 0;
			bool result = GetFileSecurity(filePath,
			SECURITY_INFORMATION.DACL_SECURITY_INFORMATION,
			IntPtr.Zero, 0, out dwLength);

			int lastError = Marshal.GetLastWin32Error();

			//Console.WriteLine(string.Format("Last Error = {0}", lastError));

			if ((result || lastError == 0x7a) && dwLength > 0) {
				PSECURITY_DESCRIPTOR pSecurityDescriptor =
				System.Runtime.InteropServices.Marshal.AllocHGlobal((int)dwLength);
				SECURITY_DESCRIPTOR descriptor = new SECURITY_DESCRIPTOR();
				Marshal.PtrToStructure(pSecurityDescriptor, descriptor);


				if (GetFileSecurity(filePath,
				  SECURITY_INFORMATION.DACL_SECURITY_INFORMATION,
				  pSecurityDescriptor, dwLength, out dwLength)) {
				}

				System.Runtime.InteropServices.Marshal.FreeHGlobal(pSecurityDescriptor);
			}


		}
		
		static public void CopyFileSecurity(string fileName,string targetFileName) {
			uint dwLength = 0;
			bool result = GetFileSecurity(fileName,
			SECURITY_INFORMATION.DACL_SECURITY_INFORMATION,
			IntPtr.Zero, 0, out dwLength);

			int lastError = Marshal.GetLastWin32Error();

			//Console.WriteLine(string.Format("Last Error = {0}", lastError));

			if ((result || lastError == 0x7a) && dwLength > 0) {
				PSECURITY_DESCRIPTOR pSecurityDescriptor =
				System.Runtime.InteropServices.Marshal.AllocHGlobal((int)dwLength);
				if (GetFileSecurity(fileName,
				  SECURITY_INFORMATION.DACL_SECURITY_INFORMATION,
				  pSecurityDescriptor, dwLength, out dwLength)) {
					SetFileSecurity(targetFileName,
					  SECURITY_INFORMATION.DACL_SECURITY_INFORMATION,
					  pSecurityDescriptor);
				}

				System.Runtime.InteropServices.Marshal.FreeHGlobal(pSecurityDescriptor);
			}
		}
	}
}
