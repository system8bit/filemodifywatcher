﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tools.FileModifyWatcher.Model.Filter {

	public class MatchFilter : IFilter {
		private string _word = string.Empty;
		private bool _isCompleteMatch = false;
		private bool _isNegative = false;

		LogPart _part = LogPart.FullPath;

		public MatchFilter(string word, bool isNegative, bool isCompleteMatch) {
			this._isNegative = isNegative;
			this._isCompleteMatch = isCompleteMatch;
			this._word = word;
		}


		public bool Check(WatchLogData log) {
			string compareString = FilterHelper.GetPart(log, LogPart.FullPath);

			if (string.IsNullOrEmpty(compareString)) {
				return false;
			}

			bool match = false;
			if (_isCompleteMatch) {
				match = compareString.Equals(_word);
			}
			else {
				match = compareString.Contains(_word);
			}

			return _isNegative ^ match;
		}

		public LogPart Part {
			get {
				return _part;
			}
			set {
				_part = value;
			}
		}


		public string MatchWord {
			get {
				return _word;
			}
			set {
				_word = value;
			}
		}


		public bool IsNagative {
			get {
				return _isNegative;
			}
			set {
				_isNegative = value;
			}
		}
	}
}
