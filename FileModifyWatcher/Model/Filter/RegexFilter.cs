﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Text.RegularExpressions;

namespace Tools.FileModifyWatcher.Model.Filter {
	public class RegexFilter : IFilter {

		string _pattern = string.Empty;
		LogPart _part = LogPart.FullPath;

		bool _isNagative;

		public string Pattern {
			get { return _pattern; }
			set { 
				_pattern = value;
				re = new Regex(_pattern, RegexOptions.Compiled);
			}
		}

		Regex re = null;

		public RegexFilter(string pattern) {
			this._pattern = pattern;
			re = new Regex(_pattern, RegexOptions.Compiled);
		}

		public bool Check(WatchLogData log) {
			string compare = FilterHelper.GetPart(log, _part);
			return IsNagative ^ re.IsMatch(compare);
		}

		public LogPart Part {
			get {
				return _part;
			}
			set {
				_part = value;
			}
		}


		public string MatchWord {
			get {
				return _pattern;
			}
			set {
				_pattern = value;
			}
		}


		public bool IsNagative {
			get {
				return _isNagative;
			}
			set {
				_isNagative = value;
			}
		}
	}
}
