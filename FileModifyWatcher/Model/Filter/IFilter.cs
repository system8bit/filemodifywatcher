﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tools.FileModifyWatcher.Model.Filter {
	public enum LogPart {
		FullPath,
		NameOnly,
		UpperDirectory,
	}

	internal class FilterHelper {
		static public string GetPart(WatchLogData log, LogPart part) {
			string ret = string.Empty;
			switch (part) {
				case LogPart.FullPath:
					ret = log.CurrentPathInfo.FullPath;
					break;

				case LogPart.NameOnly:
					ret = log.CurrentPathInfo.Name;
					break;

				case LogPart.UpperDirectory:
					ret = log.CurrentPathInfo.UpplerFolderPath;
					break;
			}
			return ret;

			
		}

	}

	public interface IFilter {
		LogPart Part { get; set; }
		bool Check(WatchLogData log);

		string MatchWord { get; set; }

		bool IsNagative { get; set; }
	}
}
